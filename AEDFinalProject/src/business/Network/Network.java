/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business.Network;

import business.Tenant.Area;
import business.enterprise.EnterpriseDirectory;

/**
 *
 * @author Vaibhavi
 */
public class Network {
    
    private String stateName;
    private EnterpriseDirectory enterpriseDirectory;
    private Area area;
    
    public Network()
    {
        enterpriseDirectory = this.enterpriseDirectory;
        area = this.area;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public Area getArea() {
        return area;
    }
}
